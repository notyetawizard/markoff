--[[ TODO:
    - read a the input file line by line
    - check the beginning of each line for beginning symbols

    - codeblocks seem special?
--]]

--------------------------------------------------------------------------------
-- Whitespace stripers ---------------------------------------------------------
--------------------------------------------------------------------------------
local function stripLeading(chunk)
	return string.match(chunk, "^%s*(.-)$")
end

local function stripTrailing(chunk)
	return string.match(chunk, "^(.-)%s*$")
end

local function stripSpace(chunk)
	if chunk == nil then return nil end
	return string.match(chunk, "^%s*(.-)%s*$")
end

--------------------------------------------------------------------------------
-- Stylizers -------------------------------------------------------------------
--------------------------------------------------------------------------------

local function setHeading(chunk)
	a, b = string.match(chunk, "(=*)(.*)")
	return "<h" .. string.len(a) .. ">" .. stripSpace(b) .. "</h" .. string.len(a) .. ">"
end

local function setParagraph(chunk)
	return "<p>" .. chunk .. "</p>"
end

local function setItalic(chunk)
	a, b, c = string.match(chunk, "(.-)//(.-)//(.*)")
	if a == nil then
		return chunk
	else
		return setItalic(a .. "<i>" .. b .. "</i>" .. c)
	end
end

local function setBold(chunk)
	a, b, c = string.match(chunk, "(.-)%*%*(.-)%*%*(.*)")
	if a == nil then
		return chunk
	else
		return setBold(a .. "<b>" .. b .. "</b>" .. c)
	end
end

--------------------------------------------------------------------------------
-- Parser ----------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Iterator for chunks of text
local function chunks(file)
	return function ()
		local line = stripSpace(file:read("l"))
		local chunk = ""
		while line and line ~= "" do
			if string.match(line, "\\", -1) then
				line = stripSpace(string.sub(line, 1, -2)) .. "</br>"
			end
			chunk = chunk .. " " .. line
			line = stripSpace(file:read("l"))
		end
		if chunk == "" and line == nil then
			return nil
		else
			chunk = stripSpace(chunk)
			return chunk
		end
	end
end

local input = io.open("README.mo", "r")
local output = io.open("test.html", "w+")
-- testing area!

for chunk in chunks(input) do
	if string.match(chunk, "^=") then
		chunk = setHeading(chunk)
	else
		chunk = setParagraph(chunk)
	end
	chunk = setItalic(chunk)
	chunk = setBold(chunk)
	output:write(chunk)
end

----
input:close()
output:close()
