//italicized//
**emboldened**

= Biggest Title

== Smaller Title

=== You get the drill

==== Probably there will be six

===== Because that's an html thing I think

====== Smallest Title

- First list item
- Second list item
-- Subitem in the second list
--- Sub item in that sub item
- Third list item
    -- Leading white space is ignored
    -- But it looks nice!

# First of a numbered list
# Second
#4 You can specify numbers if your weird

> You can use this to indent, continuing paragraphs as you please while maintaining
list indentation and all that

#5 Which makes this a good way to pick back up, actually
## You can also sublist with these

>> And indent to match

## If that's your thing

[link text and a](url or filepath) works like markdown

_____ Five of these guys for a horizontal line

You can do `inline monospace` like so and

```
    code blocks
    or whatever
    like this
```

\ escape things as you please

Like markdown, a single newline like you see
here is ignored, but leaving a blank line

like that while be meaningful. Lists do not require an extra line because they
have that sweet marker upfront do distinguish lines.

If you want to break lines on purpose, drop a\
at the end of the line to force it.

_____

Isn't it nice how everything has a unique symbol, and they don't interact weirdly
with eachother? :)
