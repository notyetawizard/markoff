local ifile = io.open(arg[1], "r")
local ofile = io.open(arg[2], "w+")
local depth = 0
local italic = false
local bold = false

local function strip(line)
	if line == nil then return nil end
	return string.match(line, "^%s*(.-)%s*$")
end

local function getLine()
    return strip(ifile:read("l"))
end

--------------------------------------------------------------------------------
-- Parsers ---------------------------------------------------------------------
--------------------------------------------------------------------------------

local function italic(line)
	local function irepl()
		if italic then
			return "</i>"
		else
			return "<i>"
		end
	end
	line = string.gsub(line, "//", irepl)
	return line
end

local function bold(line)
	local function brepl()
		if bold then
			return "</b>"
		else
			return "<b>"
		end
	end
	line = string.gsub(line, "%*%*", brepl)
	return line
end

local function emphasis(line)
	if line then
		return bold(italic(line))
	else
		local line = ""
		if italic then line = "</i>" end
		if bold then line = line .. "</b>" end
		return line
	end
end

-- Parse preformatted text
local function preformatted(line)
	ofile:write(string.rep("\t", depth) .. "<pre>\n")
	line = getLine()
	while line ~= "```" and line ~= nil do
		ofile:write(line .. "\n")
		line = getLine()
	end
	ofile:write(string.rep("\t", depth) .. "</pre>\n")
	return getLine()
end

-- Parse an ordered or unordered list
local function list(line)
	local function lines()
		while line ~= "" and line ~= nil do
	        local a, b, c = string.match(line, "^([%-#]*)(.-)(\\?)$")
			-- Check if there's a sublist, and pass it on
			local nepth = string.len(a)
	        if nepth > depth then
				depth = depth + 1
				line = list(line)
				if line ~= "" and line ~= nil then
					a, b, c = string.match(line, "^([%-#]*)(.-)(\\?)$")
					if string.len(a) < depth then break end
				else break
				end
	        elseif nepth < depth and nepth ~= 0 then
				break
			end
			-- Emphasize the line
			line = emphasis(b)
			-- Check for broken lines
			if c == "\\" then c = "</br>" end
			if a ~= "" then
				ofile:write(string.rep("\t", depth) .. "<li>" .. b .. c)
			else
				ofile:write(string.rep("\t", depth) .. b .. c)
			end
			-- Check to see if we need to close this list item
			line = getLine()
			if line == nil or string.match(line, "^[%-#]") then
				ofile:write(emphasis() .. "</li>\n")
			else
				ofile:write("\n")
			end
	    end
		return line
	end

	if string.match(line, "^%-") then
    	ofile:write(string.rep("\t", depth) .. "<ul>\n")
		line = lines(line)
		ofile:write(string.rep("\t", depth) .. "</ul>\n")
	elseif string.match(line, "^#") then
		ofile:write(string.rep("\t", depth) .. "<ol>\n")
		line = lines(line)
		ofile:write(string.rep("\t", depth) .. "</ol>\n")
	end
	depth = depth - 1
	return line
end

-- Parse headings
local function heading(line)
	local n = string.len(string.match(line, "^(=*)"))
	ofile:write(string.rep("\t", depth) .. "<h" .. n .. ">\n")
	while line ~= "" and line ~= nil do
		local b, c = string.match(line, "^(.-)(\\?)$")
		-- Emphasize the line
		line = emphasis(b)
		-- Check for broken lines
		if c == "\\" then c = "</br>" end
		ofile:write(string.rep("\t", depth) .. emphasis(b) .. c .. "\n")
		line = getLine()
	end
	ofile:write(string.rep("\t", depth) .. emphasis() .. "\n")
	ofile:write(string.rep("\t", depth) .. "</h" .. n.. ">\n")
end

local function paragraph(line)
	ofile:write(string.rep("\t", depth) .. "<p>\n")
	while line ~= "" and line ~= nil do
		local b, c = string.match(line, "^(.-)(\\?)$")
		-- Emphasize the line
		line = emphasis(b)
		-- Check for broken lines
		if c == "\\" then c = "</br>" end
		ofile:write(string.rep("\t", depth) .. emphasis(b) .. c .. "\n")
		line = getLine()
	end
	ofile:write(string.rep("\t", depth) .. emphasis() .. "\n")
	ofile:write(string.rep("\t", depth) .. "</p>\n")
end

--------------------------------------------------------------------------------
-- Begin parsing ---------------------------------------------------------------
--------------------------------------------------------------------------------

ofile:write("<body>\n")
local line = getLine()
while line ~= nil do
	depth = 1
    -- Preformatted text
    if string.match(line, "^```") then
		line = preformated(line)
    -- Lists
	elseif string.match(line, "^[%-#]") then
        line = list(line)
    -- Headings
	elseif string.match(line, "^=") then
		line = heading(line)
    -- Paragraphs
    else
		line = paragraph(line)
    end
	line = getLine()
end
ofile:write("</body>")

ifile:close()
ofile:close()
